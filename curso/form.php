<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php $curso_dto = array_key_exists('curso_dto', $GLOBALS) ? $GLOBALS['curso_dto'] : false ?>
<?php $professor_dtos = array_key_exists('professor_dtos', $GLOBALS) ? $GLOBALS['professor_dtos'] : array() ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cadastro de Curso</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p>Por favor, preencha os campos com asterisco (*), pois são obrigatórios.</p>
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/curso<?php if($curso_dto) echo '/update' ?>" method="post" onsubmit="return validate()">
                                <?php if($curso_dto) {?>
                                    <input type="hidden" name="id" value="<?php if($curso_dto) echo $curso_dto['id'] ?>">
                                <?php } ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-denominacao">
                                            <label class="control-label">Denominação*</label>
                                            <input class="form-control" name="denominacao" value="<?php if($curso_dto) echo $curso_dto['denominacao'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-tipo">
                                            <label class="control-label">Tipo*</label>
                                            <select class="form-control" name="tipo">
                                                <option value="" <?php if(!$curso_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="Curso Superior de Bacharelado" <?php if($curso_dto && $curso_dto['tipo'] == 'Curso Superior Bacharelado') echo 'selected' ?>>Curso Superior de Bacharelado</option>
                                                <option value="Curso Superior de Licenciatura" <?php if($curso_dto && $curso_dto['tipo'] == 'Curso Superior Licenciatura') echo 'selected' ?> >Curso Superior de Licenciatura</option>
                                                <option value="Curso Superior Tecnológico" <?php if($curso_dto && $curso_dto['tipo'] == 'Curso Superior Tecnológico') echo 'selected' ?> >Curso Superior Tecnológico</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group" id="div-modalidade">
                                            <label class="control-label">Modalidade*</label>
                                            <select class="form-control" name="modalidade">
                                                <option value="" <?php if(!$curso_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="A distância" <?php if($curso_dto && $curso_dto['modalidade'] == 'A distância') echo 'selected' ?>>A distância</option>
                                                <option value="Presencial" <?php if($curso_dto && $curso_dto['modalidade'] == 'Presencial') echo 'selected' ?> >Presencial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="div-turno">
                                            <label class="control-label">Turnos de funcionamento*</label>
                                            <select class="form-control" name="turno">
                                                <option value="" <?php if(!$curso_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="Diurno" <?php if($curso_dto && $curso_dto['turno'] == 'Diurno') echo 'selected' ?> >Diurno</option>
                                                <option value="Noturno" <?php if($curso_dto && $curso_dto['turno'] == 'Noturno') echo 'selected' ?> >Noturno</option>
                                                <option value="A distância" <?php if($curso_dto && $curso_dto['turno'] == 'A distância') echo 'selected' ?>>A distância</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-local">
                                            <label class="control-label">Local de oferta*</label>
                                            <select class="form-control" name="local">
                                                <option value="" <?php if(!$curso_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="Campus Giovanina Rímoli - SGAN 609 (Sede)" <?php if($curso_dto && $curso_dto['local'] == 'Campus Giovanina Rímoli - SGAN 609 (Sede)') echo 'selected' ?> >Campus Giovanina Rímoli - SGAN 609 (Sede)</option>
                                                <option value="Campus Edson Machado - SGAS Quadra 613/614" <?php if($curso_dto && $curso_dto['local'] == 'Campus Edson Machado - SGAS Quadra 613/614') echo 'selected' ?> >Campus Edson Machado - SGAS Quadra 613/614</option>
                                                <option value="Campus Liliane Barbosa - QNN 31" <?php if($curso_dto && $curso_dto['local'] == 'Campus Liliane Barbosa - QNN 31') echo 'selected' ?>>Campus Liliane Barbosa - QNN 31</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-professor">
                                            <label class="control-label">Coordenador*</label>
                                            <select class="form-control" name="professor_id" >
                                                <option value="" <?php if(!$curso_dto) echo 'selected' ?>>Selecione</option>
                                                <?php foreach ($professor_dtos as $dto) { ?>
                                                    <option value="<?php echo $dto['id'] ?>" <?php if($curso_dto && $curso_dto['professor_id'] == $dto['id']) echo 'selected' ?> ><?php echo $dto['nome'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="div-habilitacao">
                                            <label class="control-label">Habilitação*</label>
                                            <input class="form-control" name="habilitacao" value="<?php if($curso_dto) echo $curso_dto['denominacao'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3" id="div-vagas">
                                        <label class="control-label">Número de vagas para cada turno*</label>
                                        <div class="form-group input-group">
                                            <input class="form-control" name="vagas" value="<?php if($curso_dto) echo $curso_dto['vagas'] ?>">
                                            <span class="input-group-addon">vagas anuais</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group" id="div-regime-letivo">
                                            <label class="control-label">Regime letivo*</label>
                                            <select class="form-control" name="regime_letivo">
                                                <option value="" <?php if(!$curso_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="Semestral" <?php if($curso_dto && $curso_dto['regime_letivo'] == 'Semestral') echo 'selected' ?>>Semestral</option>
                                                <option value="Anual" <?php if($curso_dto && $curso_dto['regime_letivo'] == 'Anual') echo 'selected' ?> >Anual</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3" id="div-periodos">
                                        <label class="control-label">Períodos</label>
                                        <div class="form-group input-group">
                                            <input class="form-control" name="periodos" maxlength="2" value="<?php if($curso_dto) echo $curso_dto['periodos'] ?>">
                                            <span class="input-group-addon">semestres</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3" id="div-ch">
                                        <label class="control-label">Carga horária total</label>
                                        <div class="form-group input-group">
                                            <input class="form-control" name="carga_horaria" value="<?php if($curso_dto) echo $curso_dto['carga_horaria'] ?>">
                                            <span class="input-group-addon">horas</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>

                            </form>



                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
        function onlyNumbers(e){
            var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
            if (!($.inArray(e.which, key_codes) >= 0)) {
                e.preventDefault();
            }
        }

        $(function () {
            $('input[name=vagas]').keypress(onlyNumbers);
            $('input[name=periodos]').keypress(onlyNumbers);
            $('input[name=carga_horaria]').keypress(onlyNumbers);
        });

        function validate() {
            var flag = true;
            if(!$('input[name=denominacao]').first().val()){
                $('#div-denominacao').addClass('has-error');
                flag = false;
            }else{
                $('#div-denominacao').removeClass('has-error');
            }

            if(!$('select[name=tipo]').first().val()){
                $('#div-tipo').addClass('has-error');
                flag = false;
            }else{
                $('#div-tipo').removeClass('has-error');
            }

            if(!$('select[name=modalidade]').first().val()){
                $('#div-modalidade').addClass('has-error');
                flag = false;
            }else{
                $('#div-modalidade').removeClass('has-error');
            }

            if(!$('select[name=turno]').first().val()){
                $('#div-turno').addClass('has-error');
                flag = false;
            }else{
                $('#div-turno').removeClass('has-error');
            }

            if(!$('select[name=local]').first().val()){
                $('#div-local').addClass('has-error');
                flag = false;
            }else{
                $('#div-local').removeClass('has-error');
            }

            if(!$('select[name=professor_id]').first().val()){
                $('#div-professor').addClass('has-error');
                flag = false;
            }else{
                $('#div-professor').removeClass('has-error');
            }

            if(!$('input[name=habilitacao]').first().val()){
                $('#div-habilitacao').addClass('has-error');
                flag = false;
            }else{
                $('#div-habilitacao').removeClass('has-error');
            }

            if(!$('input[name=vagas]').first().val()){
                $('#div-vagas').addClass('has-error');
                flag = false;
            }else{
                $('#div-vagas').removeClass('has-error');
            }

            if(!$('select[name=regime_letivo]').first().val()){
                $('#div-regime-letivo').addClass('has-error');
                flag = false;
            }else{
                $('#div-regime-letivo').removeClass('has-error');
            }

            var p = $('input[name=periodos]').first().val();
            if(!p){
                $('#div-periodos').addClass('has-error');
                flag = false;
            }else if (p && (p <= 0 || p > 10)) {
                $('#div-periodos').addClass('has-error');
                $('#custom-p').html("A quantidade de períodos deve ser entre 1 e 10 semestres.");
                flag = false;
            }else{
                $('#div-periodos').removeClass('has-error');
            }

            var ch = $('input[name=carga_horaria]').first().val();
            if(!ch){
                $('#div-ch').addClass('has-error');
                flag = false;
            }else if(ch && ch <= 0){
                $('#div-ch').addClass('has-error');
                $('#custom-p').html("A carga horária deve ser maior do que zero.");
                flag = false;
            }else{
                $('#div-ch').removeClass('has-error');
            }

            if(!flag){
                $('#div-notification').removeClass('hidden');
            }

            return flag;
        }
    </script>

</body>

</html>
