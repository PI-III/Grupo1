<!DOCTYPE html>
<html lang="pt-br">

<?php require __DIR__.'/../template/_header.php' ?>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <img src="/template/img/sisppc.png" alt="sisppc" style="margin-top:12%; margin-left:30%;width: 40%;">
                <div class="panel panel-default" style="margin-top:5%">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align:center;">Por favor, faça o seu login</h3>
                    </div>
                    <div class="panel-body">

                        <!-- Notification -->
                        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_notification.php' ?>

                        <form role="form" method="post" action="/login">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Nome de usuário" name="nome" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="senha" type="password" value="">
                                </div>
                                <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require __DIR__.'/../template/_script.php' ?>

</body>

</html>
