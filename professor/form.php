<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php $professor_dto = array_key_exists('professor_dto', $GLOBALS) ? $GLOBALS['professor_dto'] : false ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cadastro de Professor</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p>Por favor, preencha os campos com asterisco (*), pois são obrigatórios.</p>
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/professor<?php if($professor_dto) echo '/update' ?>" method="post" onsubmit="return validate()">
                                <?php if($professor_dto) {?>
                                    <input type="hidden" name="id" value="<?php if($professor_dto) echo $professor_dto['id'] ?>">
                                <?php } ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-nome">
                                            <label class="control-label">Nome*</label>
                                            <input class="form-control" name="nome" value="<?php if($professor_dto) echo $professor_dto['nome'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-cpf">
                                            <label class="control-label">CPF*</label>
                                            <input maxlength="11" class="form-control" name="cpf" value="<?php if($professor_dto) echo $professor_dto['cpf'] ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-titulacao">
                                            <label class="control-label">Maior titulação*</label>
                                            <select class="form-control" name="titulacao">
                                                <option value="" <?php if(!$professor_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="Graduação" <?php if($professor_dto && $professor_dto['titulacao'] == 'Graduação') echo 'selected' ?>>Graduação</option>
                                                <option value="Pós-Graduação" <?php if($professor_dto && $professor_dto['titulacao'] == 'Pós-Graduação') echo 'selected' ?> >Pós-Graduação</option>
                                                <option value="Mestrado" <?php if($professor_dto && $professor_dto['titulacao'] == 'Mestrado') echo 'selected' ?> >Mestrado</option>
                                                <option value="Doutorado" <?php if($professor_dto && $professor_dto['titulacao'] == 'Doutorado') echo 'selected' ?> >Doutorado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-formacao">
                                            <label class="control-label">Área de formação*</label>
                                            <input class="form-control" name="formacao" value="<?php if($professor_dto) echo $professor_dto['formacao'] ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-curriculo">
                                            <label class="control-label">Link do currículo lattes*</label>
                                            <input class="form-control" name="curriculo" value="<?php if($professor_dto) echo $professor_dto['curriculo'] ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>

                            </form>



                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
        $(function () {
            $('input[name=cpf]').keypress(function(e) {
                var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
                if (!($.inArray(e.which, key_codes) >= 0)) {
                    e.preventDefault();
                }
            });
        });
        function validate() {
            var flag = true;
            if(!$('input[name=nome]').first().val()){
                $('#div-nome').addClass('has-error');
                flag = false;
            }else{
                $('#div-nome').removeClass('has-error');
            }

            if(!$('input[name=cpf]').first().val()){
                $('#div-cpf').addClass('has-error');
                flag = false;
            }else{
                $('#div-cpf').removeClass('has-error');
            }

            if(!$('select[name=titulacao]').first().val()){
                $('#div-titulacao').addClass('has-error');
                flag = false;
            }else{
                $('#div-titulacao').removeClass('has-error');
            }

            if(!$('input[name=formacao]').first().val()){
                $('#div-formacao').addClass('has-error');
                flag = false;
            }else{
                $('#div-formacao').removeClass('has-error');
            }

            if(!$('input[name=curriculo]').first().val()){
                $('#div-curriculo').addClass('has-error');
                flag = false;
            }else{
                $('#div-curriculo').removeClass('has-error');
            }

            if(!flag){
                $('#div-notification').removeClass('hidden');
            }

            return flag;
        }
    </script>

</body>

</html>
