<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php $ata_dto = array_key_exists('ata_dto', $GLOBALS) ? $GLOBALS['ata_dto'] : false ?>
<?php $ata_professor_dtos = array_key_exists('ata_professor_dtos', $GLOBALS) ? $GLOBALS['ata_professor_dtos'] : false ?>
<?php $professor_dtos = array_key_exists('professor_dtos', $GLOBALS) ? $GLOBALS['professor_dtos'] : array() ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cadastro de Atas de Reunião</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p>Por favor, preencha os campos com asterisco (*), pois são obrigatórios.</p>
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/ata<?php if($ata_dto) echo '/update' ?>" method="post" onsubmit="return validate()">
                                <?php if($ata_dto) {?>
                                    <input type="hidden" name="id" value="<?php if($ata_dto) echo $ata_dto['id'] ?>">
                                <?php } ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-pauta">
                                            <label class="control-label">Pauta*</label>
                                            <input class="form-control" name="pauta" value="<?php if($ata_dto) echo $ata_dto['pauta'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group" id="div-data">
                                            <label class="control-label">Data*</label>
                                            <input class="form-control" name="data" maxlength="10" value="<?php if($ata_dto) echo $ata_dto['data'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group" id="div-local">
                                            <label class="control-label">Local*</label>
                                            <select class="form-control" name="local">
                                                <option value="" <?php if(!$ata_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="Campus Giovanina Rímoli - SGAN 609 (Sede)" <?php if($ata_dto && $ata_dto['local'] == 'Campus Giovanina Rímoli - SGAN 609 (Sede)') echo 'selected' ?> >Campus Giovanina Rímoli - SGAN 609 (Sede)</option>
                                                <option value="Campus Edson Machado - SGAS Quadra 613/614" <?php if($ata_dto && $ata_dto['local'] == 'Campus Edson Machado - SGAS Quadra 613/614') echo 'selected' ?> >Campus Edson Machado - SGAS Quadra 613/614</option>
                                                <option value="Campus Liliane Barbosa - QNN 31" <?php if($ata_dto && $ata_dto['local'] == 'Campus Liliane Barbosa - QNN 31') echo 'selected' ?>>Campus Liliane Barbosa - QNN 31</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-professor">
                                            <label class="control-label">Participantes*</label>
                                            <select multiple class="form-control" name="professor[]" id="professor" size="6" >
                                                <?php foreach ($professor_dtos as $dto) { ?>
                                                    <option value="<?php echo $dto['id'] ?>" <?php if($ata_dto && in_array($dto['id'], array_column($ata_professor_dtos, 'professor_id'))) echo 'selected' ?> ><?php echo $dto['nome'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-deliberacao">
                                            <label class="control-label">Deliberações*</label>
                                            <textarea class="form-control" name="deliberacao" rows="5" maxlength="500"><?php if($ata_dto) echo $ata_dto['deliberacao'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>

                            </form>



                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
        $(function () {
            $('input[name=data]').keypress(function(e) {
                var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
                if (!($.inArray(e.which, key_codes) >= 0)) {
                    e.preventDefault();
                }else{
                    var data = $('input[name=data]').first().val();
                    if(data.length == 2 || data.length == 5){
                        $('input[name=data]').first().val(data+'/');
                    }
                }
            });
        });
        function validate() {
            var flag = true;
            var inputs = [
                {type:'input', name: 'pauta', div: 'div-pauta'},
                {type: 'input', name: 'data', div: 'div-data'},
                {type: 'select', name: 'local', div: 'div-local'},
                {type: 'select', name: 'professor', div: 'div-professor', findById: true},
                {type: 'textarea', name: 'deliberacao', div: 'div-deliberacao'}
            ];

            inputs.forEach(function(e){
                if(isEmpty(e)){
                    flag = false;
                }
            });

            var data = $('input[name=data]').first().val();
            if(!data || data.length < 10){
                $('#div-data').addClass('has-error');
                flag = false;
            }else{
                var array = data.split('/');
                var dia = parseInt(array[0]);
                var mes = parseInt(array[1]);
                var ano = parseInt(array[2]);
                if(dia > 31 || mes > 12 || (ano < 2000)){
                    $('#div-data').addClass('has-error');
                    $('#custom-p').html("A data informada é inválida");
                    flag = false;
                }else{
                    $('#div-data').removeClass('has-error');
                }
            }

            if(!flag){
                $('#div-notification').removeClass('hidden');
            }

            return flag;
        }

        function isEmpty(e) {
            var query = e.findById ? '#'+e.name : e.type +'[name='+e.name+']';
            if($(query).first().val() == ""){
                $('#'+e.div).addClass('has-error');
                return true;
            }else{
                $('#'+e.div).removeClass('has-error');
                return false;
            }
        }
    </script>

</body>

</html>
