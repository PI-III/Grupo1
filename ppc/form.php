<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php
$ppc_dto = array_key_exists('ppc_dto', $GLOBALS) ? $GLOBALS['ppc_dto'] : false;
$curso_dto = array_key_exists('curso_dto', $GLOBALS) ? $GLOBALS['curso_dto'] : false;
$curso_dtos = array_key_exists('curso_dtos', $GLOBALS) ? $GLOBALS['curso_dtos'] : false;
?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cadastro de Projeto Pedagógico de Curso</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p>Por favor, preencha os campos com asterisco (*), pois são obrigatórios.</p>
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/ppc<?php if($ppc_dto) echo '/update' ?>" method="post" onsubmit="return validate()">
                                <?php if($ppc_dto) {?>
                                    <input type="hidden" name="id" value="<?php if($ppc_dto) echo $ppc_dto['id'] ?>">
                                <?php } ?>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-curso">
                                            <label class="control-label">Curso*</label>
                                            <?php if(!$curso_dto){ ?>
                                                <select class="form-control" name="curso_id">
                                                    <option value="" selected>Selecione</option>
                                                    <?php foreach ($curso_dtos as $dto) { ?>
                                                        <option value="<?php echo $dto['id'] ?>">
                                                            <?php echo $dto['denominacao'] ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            <?php }else{ ?>
                                                <p id="curso-id" class="form-control-static">
                                                    <?php echo $curso_dto['denominacao'] ?>
                                                </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-perfil-curso">
                                            <label class="control-label">Perfil do curso*</label>
                                            <textarea class="form-control" name="perfil_curso" rows="5" maxlength="500"><?php if($ppc_dto) echo trim($ppc_dto['perfil_curso']) ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-perfil-egresso">
                                            <label class="control-label">Perfil do egresso*</label>
                                            <textarea class="form-control" name="perfil_egresso" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['perfil_egresso'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-acesso">
                                            <label class="control-label">Forma de acesso ao curso*</label>
                                            <textarea class="form-control" name="acesso" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['acesso'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-perfil-formacao">
                                            <label class="control-label">Representação gráfica de um perfil de formação*</label>
                                            <textarea class="form-control" name="perfil_formacao" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['perfil_formacao'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-avaliacao-processo">
                                            <label class="control-label">Sistema de avaliação do processo ensino aprendizagem*</label>
                                            <textarea class="form-control" name="avaliacao_processo" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['avaliacao_processo'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-avaliacao-projeto">
                                            <label class="control-label">Sistema de avaliação do projeto do curso*</label>
                                            <textarea class="form-control" name="avaliacao_projeto" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['avaliacao_projeto'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-trabalho-conclusao">
                                            <label class="control-label">Trabalho de conclusão de curso*</label>
                                            <textarea class="form-control" name="trabalho_conclusao" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['trabalho_conclusao'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-estagio">
                                            <label class="control-label">Estágio curricular*</label>
                                            <textarea class="form-control" name="estagio" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['estagio'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-politica-inclusao">
                                            <label class="control-label">Política de atendimento a pessoas com deficiência e/ou mobilidade reduzida*</label>
                                            <textarea class="form-control" name="politica_inclusao" rows="5" maxlength="500"><?php if($ppc_dto) echo $ppc_dto['politica_inclusao'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>

                            </form>



                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
        function validate() {
            var flag = true;
            <?php if(!$curso_dto){ ?>
                if(!$('select[name=curso_id]').first().val()){
                    $('#div-curso').addClass('has-error');
                    flag = false;
                }else{
                    $('#div-curso').removeClass('has-error');
                }
            <?php } ?>
            if(!$('textarea[name=perfil_curso]').first().val()){
                $('#div-perfil-curso').addClass('has-error');
                flag = false;
            }else{
                $('#div-perfil-curso').removeClass('has-error');
            }

            if(!$('textarea[name=perfil_egresso]').first().val()){
                $('#div-perfil-egresso').addClass('has-error');
                flag = false;
            }else{
                $('#div-perfil-egresso').removeClass('has-error');
            }

            if(!$('textarea[name=acesso]').first().val()){
                $('#div-acesso').addClass('has-error');
                flag = false;
            }else{
                $('#div-acesso').removeClass('has-error');
            }

            if(!$('textarea[name=perfil_formacao]').first().val()){
                $('#div-perfil-formacao').addClass('has-error');
                flag = false;
            }else{
                $('#div-perfil-formacao').removeClass('has-error');
            }

            if(!$('textarea[name=avaliacao_processo]').first().val()){
                $('#div-avaliacao-processo').addClass('has-error');
                flag = false;
            }else{
                $('#div-avaliacao-processo').removeClass('has-error');
            }

            if(!$('textarea[name=avaliacao_projeto]').first().val()){
                $('#div-avaliacao-projeto').addClass('has-error');
                flag = false;
            }else{
                $('#div-avaliacao-projeto').removeClass('has-error');
            }

            if(!$('textarea[name=trabalho_conclusao]').first().val()){
                $('#div-trabalho-conclusao').addClass('has-error');
                flag = false;
            }else{
                $('#div-trabalho-conclusao').removeClass('has-error');
            }

            if(!$('textarea[name=estagio]').first().val()){
                $('#div-estagio').addClass('has-error');
                flag = false;
            }else{
                $('#div-estagio').removeClass('has-error');
            }

            if(!$('textarea[name=estagio]').first().val()){
                $('#div-estagio').addClass('has-error');
                flag = false;
            }else{
                $('#div-estagio').removeClass('has-error');
            }

            if(!$('textarea[name=politica_inclusao]').first().val()){
                $('#div-politica-inclusao').addClass('has-error');
                flag = false;
            }else{
                $('#div-politica-inclusao').removeClass('has-error');
            }

            if(!flag){
                $('#div-notification').removeClass('hidden');
            }

            return flag;
        }
    </script>

</body>

</html>
