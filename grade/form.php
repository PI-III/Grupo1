<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php
$grade_dtos = array_key_exists('grade_dtos', $GLOBALS) ? $GLOBALS['grade_dtos'] : false;
$curso_dto = array_key_exists('curso_dto', $GLOBALS) ? $GLOBALS['curso_dto'] : false;
$curso_dtos = array_key_exists('curso_dtos', $GLOBALS) ? $GLOBALS['curso_dtos'] : false;
$disciplina_dtos = $GLOBALS['disciplina_dtos'];
?>

<body>
    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cadastro de Grade Curricular</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p>Por favor, preencha os campos com asterisco (*), pois são obrigatórios.</p>
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/grade<?php if($curso_dto) echo '/update' ?>" method="post" <?php if(!$curso_dto){?>  onsubmit="return validate()" <?php } ?>>
                                <?php if($curso_dto) {?>
                                    <input type="hidden" name="curso_id" value="<?php if($curso_dto) echo $curso_dto['id'] ?>">
                                <?php } ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group" id="div-curso">
                                                <label>Curso</label>
                                                <?php if(!$curso_dto){ ?>
                                                    <select class="form-control" name="curso_id" onchange="selectCourse()">
                                                        <option value="" data-ch="0" data-p="1" selected>Selecione</option>
                                                        <?php foreach ($curso_dtos as $dto) { ?>
                                                            <option value="<?php echo $dto['id'] ?>"
                                                                data-p="<?php echo $dto['periodos'] ?>"
                                                                data-ch="<?php echo $dto['carga_horaria'] ?>">
                                                                <?php echo $dto['denominacao'] ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                <?php }else{ ?>
                                                    <p id="curso-id"
                                                        data-ch="<?php echo $curso_dto['carga_horaria'] ?>"
                                                        data-p="<?php echo $curso_dto['periodos'] ?>"
                                                        class="form-control-static">
                                                        <?php echo $curso_dto['denominacao'] ?>
                                                    </p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>Semestres</label>
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-pills">
                                                <?php for ($i = 1; $i <= 10; $i++) { ?>
                                                    <li class="<?php if($i == 1){echo 'active';}else{echo 'hidden';} ?>" id="tab-<?php echo $i ?>" >
                                                        <a href="#<?php echo $i ?>-pills" data-toggle="tab" onclick="selectTab(this)">
                                                            <?php echo $i ?>º
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <?php for ($i = 1; $i <= 10 ; $i++) { ?>
                                                    <div class="tab-pane fade <?php if($i == 1) echo 'in active' ?>" id="<?php echo $i ?>-pills">
                                                        <br>
                                                        <div class="form-group">
                                                            <label>Disciplinas</label>
                                                            <select multiple="" class="form-control" name="periodo_<?php echo $i ?>[]" id="periodo_<?php echo $i ?>">
                                                                <?php foreach ($disciplina_dtos as $dto) { ?>
                                                                    <option value="<?php echo $dto['id'] ?>"
                                                                        data-ch="<?php echo $dto['carga_horaria'] ?>"
                                                                        <?php
                                                                        if($curso_dto && in_array($i, array_column($grade_dtos, 'periodo'))){
                                                                            foreach ($grade_dtos as $key => $cd) {
                                                                                if($cd['periodo'] == $i && $cd['disciplina_id'] == $dto['id']){
                                                                                    echo 'selected';
                                                                                }
                                                                            }
                                                                        }

                                                                        ?>
                                                                        onclick="selectDiscipline(this)">
                                                                        <?php echo $dto['nome'].' | '.$dto['carga_horaria'].'h' ?>
                                                                    </option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-4">
                                            <br><br>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Carga Horária</th>
                                                            <th>Valor</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Deste semestre até o momento</td>
                                                            <td id="p-current">0h</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Do curso até o momento</td>
                                                            <td id="c-current">0h</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total</td>
                                                            <td id="t-current">0h</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <button type="submit" class="btn btn-primary">Salvar</button>
                                        </div>
                                    </div>

                                </form>



                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <script>
            function selectDiscipline(e) {
                var periodo = $(e).parent().attr('id');
                $('#p-current').html(calculateCH(periodo)+"h");
                calculateCCH();
            }

            function selectTab(e) {
                var href = $(e).attr('href');
                var tabNumber = href.split('-')[0].split('#')[1];
                $('#p-current').html(calculateCH('periodo_'+tabNumber)+"h");
            }

            function calculateCH(periodo) {
                var options = $('#'+periodo+' option:selected');
                var ch = 0;
                for (var i = 0; i < options.length; i++) {
                    ch += parseInt($(options[i]).attr('data-ch'));
                }
                return ch;
            }

            function calculateCCH() {
                var options = $('select[multiple] option:selected');
                var ch = 0;
                for (var i = 0; i < options.length; i++) {
                    ch += parseInt($(options[i]).attr('data-ch'));
                }
                $('#c-current').html(ch+"h");
            }

            function selectCourse() {
                var selected = $('select[name=curso_id] option:selected');
                var tch = $(selected).attr('data-ch');
                var p = $(selected).attr('data-p');
                $('#t-current').html(tch+"h");
                $('#p-current').html("0h");
                $('#c-current').html("0h");

                for(var i = 2; i <= 10; i++){
                    if(i <= p){
                        $("#tab-"+i).removeClass('hidden active');
                    }else{
                        $("#tab-"+i).addClass('hidden');
                    }
                }

                $('select[multiple] option:selected').prop('selected', false);
                $("#tab-1").addClass('active');
            }
        </script>
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
        <?php if($curso_dto){ ?>
            <script>
                (function(){
                    $('#t-current').html($('#curso-id').attr('data-ch')+"h");

                    for(var i = 2; i <= 10; i++){
                        if(i <= $('#curso-id').attr('data-p')){
                            $("#tab-"+i).removeClass('hidden active');
                        }else{
                            $("#tab-"+i).addClass('hidden');
                        }
                    }

                    $('#p-current').html(calculateCH('periodo_1')+"h");
                    calculateCCH();
                })()


            </script>
        <?php } ?>
        <?php if(!$curso_dto){ ?>
            <script>
                function validate() {
                    var flag = true;
                    if(!$('select[name=curso_id]').first().val()){
                        $('#div-curso').addClass('has-error');
                        flag = false;
                    }else{
                        $('#div-curso').removeClass('has-error');
                    }

                    if(!flag){
                        $('#div-notification').removeClass('hidden');
                    }

                    return flag;
                }
            </script>
        <?php } ?>

    </body>

    </html>
