<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php
$curso_dtos = $GLOBALS['curso_dtos'];
$ch_dtos = $GLOBALS['ch_dtos'];
?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header" style="padding-bottom: 0px;">
                        <strong class="h3" style="margin-right: 5%">Grades Curriculares</strong>
                        <a href="/grade/novo" class="btn btn-default btn-sm" style="margin-right: 5px; margin-bottom: 10px;">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </a>
                        <a href="#" class="btn btn-default btn-sm" style="margin-right: 5px; margin-bottom: 10px;" data-toggle="modal" data-target="#myModal">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_notification.php' ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table width="100%" class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Denominação do curso</th>
                                        <th>Semestres</th>
                                        <th>Carga horária atual</th>
                                        <th>Carga horária total</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($curso_dtos as $dto) { ?>
                                        <tr>
                                            <td><?php echo $dto['id'] ?></td>
                                            <td><?php echo $dto['denominacao'] ?></td>
                                            <td><?php echo $dto['periodos'] ?></td>
                                            <td><?php echo $ch_dtos[array_search($dto['id'], array_column($ch_dtos, 'curso_id'))]['ch_atual'] ?></td>
                                            <td><?php echo $dto['carga_horaria'] ?></td>
                                            <td class="center">
                                                <a href="/grade/update/<?php echo $dto['id'] ?>"><i class="fa fa-pencil fa-fw"></i></a>
                                                <a href="/grade/delete/<?php echo $dto['id'] ?>"><i class="fa fa-times fa-fw"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form role="form" action="/grade/search" method="post">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Pesquisa de Grades Curriculares</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>Curso</label>
                                                            <input class="form-control" name="denominacao">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>Semestres</label>
                                                            <input class="form-control" name="periodos">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                <button type="submit" class="btn btn-primary">Pesquisar</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </form>
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true,
            columnDefs: [{ orderable: false, targets: 5}],
            language: {
                lengthMenu: "Registros por página: _MENU_ ",
                zeroRecords: "Nenhum registro encontrado",
                info: "Página _PAGE_ de _PAGES_",
                infoEmpty: "Nenum registro disponível",
                infoFiltered: "(filtered from _MAX_ total records)",
                search: "Filtrar",
                paginate: {
                    first:"Primeiro",
                    last:"Último",
                    next:"Próximo",
                    previous: "Anterior"
                }
            }
        });
    });
    </script>

</body>

</html>
