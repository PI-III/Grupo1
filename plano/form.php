<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php $plano_dto = array_key_exists('plano_dto', $GLOBALS) ? $GLOBALS['plano_dto'] : false ?>
<?php $professor_dtos = array_key_exists('professor_dtos', $GLOBALS) ? $GLOBALS['professor_dtos'] : array() ?>
<?php $disciplina_dtos = array_key_exists('disciplina_dtos', $GLOBALS) ? $GLOBALS['disciplina_dtos'] : array() ?>
<?php $curso_dtos = array_key_exists('curso_dtos', $GLOBALS) ? $GLOBALS['curso_dtos'] : array() ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cadastro de Planos de Ensino</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p>Por favor, preencha os campos com asterisco (*), pois são obrigatórios.</p>
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/plano<?php if($plano_dto) echo '/update' ?>" method="post" onsubmit="return validate()">
                                <?php if($plano_dto) {?>
                                    <input type="hidden" name="id" value="<?php if($plano_dto) echo $plano_dto['id'] ?>">
                                <?php } ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-disciplina">
                                            <label class="control-label">Disciplina*</label>
                                            <select class="form-control" name="disciplina_id" >
                                                <option value="" <?php if(!$plano_dto) echo 'selected' ?>>Selecione</option>
                                                <?php foreach ($disciplina_dtos as $dto) { ?>
                                                    <option value="<?php echo $dto['id'] ?>" <?php if($plano_dto && $plano_dto['disciplina_id'] == $dto['id']) echo 'selected' ?> ><?php echo $dto['nome'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-curso">
                                            <label class="control-label">Curso*</label>
                                            <select class="form-control" name="curso_id" >
                                                <option value="" <?php if(!$plano_dto) echo 'selected' ?>>Selecione</option>
                                                <?php foreach ($curso_dtos as $dto) { ?>
                                                    <option value="<?php echo $dto['id'] ?>" <?php if($plano_dto && $plano_dto['curso_id'] == $dto['id']) echo 'selected' ?> ><?php echo $dto['denominacao'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-professor">
                                            <label class="control-label">Professor*</label>
                                            <select class="form-control" name="professor_id" >
                                                <option value="" <?php if(!$plano_dto) echo 'selected' ?>>Selecione</option>
                                                <?php foreach ($professor_dtos as $dto) { ?>
                                                    <option value="<?php echo $dto['id'] ?>" <?php if($plano_dto && $plano_dto['professor_id'] == $dto['id']) echo 'selected' ?> ><?php echo $dto['nome'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="div-ano">
                                            <label class="control-label">Ano*</label>
                                            <input class="form-control" name="ano" value="<?php if($plano_dto) echo $plano_dto['ano'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group" id="div-semestre">
                                            <label class="control-label">Semestre letivo*</label>
                                            <select class="form-control" name="semestre">
                                                <option value="" <?php if(!$plano_dto) echo 'selected' ?>>Selecione</option>
                                                <option value="1" <?php if($plano_dto && $plano_dto['semestre'] == '1') echo 'selected' ?> >1ª</option>
                                                <option value="2" <?php if($plano_dto && $plano_dto['semestre'] == '2') echo 'selected' ?> >2ª</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-ementa">
                                            <label class="control-label">Ementa*</label>
                                            <textarea class="form-control" name="ementa" rows="5" maxlength="500"><?php if($plano_dto) echo trim($plano_dto['ementa']) ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-competencias">
                                            <label class="control-label">Comptetências e habilidades*</label>
                                            <textarea class="form-control" name="competencias" rows="5" maxlength="500"><?php if($plano_dto) echo $plano_dto['competencias'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-metodologia">
                                            <label class="control-label">Metodologia de ensino*</label>
                                            <textarea class="form-control" name="metodologia" rows="5" maxlength="500"><?php if($plano_dto) echo trim($plano_dto['metodologia']) ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-cronograma">
                                            <label class="control-label">Cronograma de atividades*</label>
                                            <textarea class="form-control" name="cronograma" rows="5" maxlength="500"><?php if($plano_dto) echo $plano_dto['cronograma'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-avaliacao">
                                            <label class="control-label">Avaliação*</label>
                                            <textarea class="form-control" name="avaliacao" rows="5" maxlength="500"><?php if($plano_dto) echo trim($plano_dto['avaliacao']) ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-bibliografia">
                                            <label class="control-label">Biliografia básica e complementar*</label>
                                            <textarea class="form-control" name="bibliografia" rows="5" maxlength="500"><?php if($plano_dto) echo $plano_dto['bibliografia'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>

                            </form>



                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
    $(function () {
        $('input[name=ano]').keypress(function(e) {
            var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
            if (!($.inArray(e.which, key_codes) >= 0)) {
                e.preventDefault();
            }
        });
    });
    function validate() {
        var flag = true;
        var inputs = [
            {type:'select', name: 'disciplina_id', div: 'div-disciplina'},
            {type:'select', name: 'curso_id', div: 'div-curso'},
            {type:'select', name: 'professor_id', div: 'div-professor'},
            {type:'input', name: 'ano', div: 'div-ano'},
            {type:'select', name: 'semestre', div: 'div-semestre'},
            {type: 'textarea', name: 'ementa', div: 'div-ementa'},
            {type: 'textarea', name: 'competencias', div: 'div-competencias'},
            {type: 'textarea', name: 'metodologia', div: 'div-metodologia'},
            {type: 'textarea', name: 'cronograma', div: 'div-cronograma'},
            {type: 'textarea', name: 'avaliacao', div: 'div-avaliacao'},
            {type: 'textarea', name: 'bibliografia', div: 'div-bibliografia'},
        ];

        inputs.forEach(function(e){
            if(isEmpty(e)){
                flag = false;
            }
        });

        var ano = $('input[name=ano]').first().val();
        if(!ano){
            $('#div-ano').addClass('has-error');
            flag = false;
        }else if(ano && (ano <= 2016 || ano > 2100)){
            $('#div-ano').addClass('has-error');
            $('#custom-p').html("O ano deve ser entre 2017 e 2100.");
            flag = false;
        }else{
            $('#div-ano').removeClass('has-error');
        }

        if(!flag){
            $('#div-notification').removeClass('hidden');
        }

        return flag;
    }

    function isEmpty(e) {
        var query = e.findById ? '#'+e.name : e.type +'[name='+e.name+']';
        if($(query).first().val() == ""){
            $('#'+e.div).addClass('has-error');
            return true;
        }else{
            $('#'+e.div).removeClass('has-error');
            return false;
        }
    }
    </script>

</body>

</html>
