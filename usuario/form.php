<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php $usuario_dto = $GLOBALS['usuario_dto']?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Perfil de Usuário</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/usuario/update" method="post" onsubmit="return validate()">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nome de usuário</label>
                                            <p class="form-control-static"><?php echo $usuario_dto['nome'] ?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Nova senha*</label>
                                            <input class="form-control" name="senha" type="password">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Confirmação de nova senha*</label>
                                            <input class="form-control" name="confirmacao_senha" type="password">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </div>
                                </div>

                            </form>



                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
        function validate() {
            var senha = $('input[name=senha]').first().val().trim();
            var c_senha = $('input[name=confirmacao_senha]').first().val().trim();

            if(!senha || !c_senha || senha != c_senha ){
                $('#div-notification').removeClass('hidden');
                $('#custom-p').html("A senha e a confirmação de senha devem ser iguais");
                return false;
            }

            if(senha.length < 8){
                $('#div-notification').removeClass('hidden');
                $('#custom-p').html("A senha deve possuir pelo menos 8 caracteres");
                return false;
            }

            if(/\s/.test(senha)){
                $('#div-notification').removeClass('hidden');
                $('#custom-p').html("A senha não deve possuir espaços em branco");
                return false;
            }

            if(!/\d\D*\d\D*\d/.test(senha)){
                $('#div-notification').removeClass('hidden');
                $('#custom-p').html("A senha deve possuir pelo menos 3 números");
                return false;
            }

            if(!/\D\d*\D\d*\D/.test(senha)){
                $('#div-notification').removeClass('hidden');
                $('#custom-p').html("A senha deve possuir pelo menos 3 letras");
                return false;
            }

            return true;
        }
    </script>

</body>

</html>
