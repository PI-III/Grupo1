<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php $disciplina_dto = array_key_exists('disciplina_dto', $GLOBALS) ? $GLOBALS['disciplina_dto'] : false ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cadastro de Disciplina</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert alert-danger hidden">
                <p>Por favor, preencha os campos com asterisco (*), pois são obrigatórios.</p>
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/disciplina<?php if($disciplina_dto) echo '/update' ?>" method="post" onsubmit="return validate()">
                                <?php if($disciplina_dto) {?>
                                    <input type="hidden" name="id" value="<?php if($disciplina_dto) echo $disciplina_dto['id'] ?>">
                                <?php } ?>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="div-nome">
                                            <label class="control-label">Nome*</label>
                                            <input class="form-control" name="nome" value="<?php if($disciplina_dto) echo $disciplina_dto['nome'] ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group" id="div-codigo">
                                            <label class="control-label">Código*</label>
                                            <input class="form-control" name="codigo" value="<?php if($disciplina_dto) echo $disciplina_dto['codigo'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3" id="div-ch">
                                        <label class="control-label">Carga horária*</label>
                                        <div class="form-group input-group">
                                            <input class="form-control" name="carga_horaria" value="<?php if($disciplina_dto) echo $disciplina_dto['carga_horaria'] ?>">
                                            <span class="input-group-addon">horas</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>

                            </form>



                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
        $(function () {
            $('input[name=carga_horaria]').keypress(function(e) {
                var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
                if (!($.inArray(e.which, key_codes) >= 0)) {
                    e.preventDefault();
                }
            });
        });
        function validate() {
            var flag = true;
            if(!$('input[name=nome]').first().val()){
                $('#div-nome').addClass('has-error');
                flag = false;
            }else{
                $('#div-nome').removeClass('has-error');
            }

            if(!$('input[name=codigo]').first().val()){
                $('#div-codigo').addClass('has-error');
                flag = false;
            }else{
                $('#div-codigo').removeClass('has-error');
            }

            var ch = $('input[name=carga_horaria]').first().val();
            if(!ch){
                $('#div-ch').addClass('has-error');
                flag = false;
            }else if(ch && ch <= 0){
                $('#div-ch').addClass('has-error');
                $('#custom-p').html("A carga horária deve ser maior do que zero.");
                flag = false;
            }else{
                $('#div-ch').removeClass('has-error');
            }

            if(!flag){
                $('#div-notification').removeClass('hidden');
            }

            return flag;
        }
    </script>

</body>

</html>
