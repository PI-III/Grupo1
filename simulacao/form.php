<!DOCTYPE html>
<html lang="pt-br">

<?php require $_SERVER['DOCUMENT_ROOT'].'/template/_header.php' ?>
<?php $curso_dtos = $GLOBALS['curso_dtos'] ?>

<style>
    .max-size{
        max-height: 690px;
    }
</style>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_navigation.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Simulação de Avaliação do MEC</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Notification -->
            <div id="div-notification" class="alert hidden">
                <p id="custom-p"></p>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <form role="form" action="/ppc<?php if($ppc_dto) echo '/update' ?>" method="post" onsubmit="return validate()">
                                <?php if($ppc_dto) {?>
                                    <input type="hidden" name="id" value="<?php if($ppc_dto) echo $ppc_dto['id'] ?>">
                                <?php } ?>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Dimensão: Organização Didático-Pedagógica
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body pre-scrollable max-size">
                                                <p>Categoria de análise: Projeto Pedagógico do Curso</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Contexto Educacional</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Autoavaliação</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Objetivos do Curso</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Perfil Profissional do Egresso</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Número de Vagas</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->

                                                <p>Categoria de análise: Projeto Pedagógico do Curso: formação (fontes de consulta: PPC e DCNs)</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Estrutura Curricular</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Conteúdos Curriculares</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Metodologia</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Atendimento ao discente</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Dimensão: Corpo Docente
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body pre-scrollable max-size">
                                                <p>Categoria de análise: Administração Acadêmica</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Composição do Núcleo Docente Estruturante – NDE</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Titulação do NDE</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Experiência profissional do NDE</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Regime de trabalho do NDE</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Titulação, formação acadêmica e experiência do coordenador do curso</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Regime de trabalho do coordenador do curso</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Composição e funcionamento do colegiado de curso ou equivalente</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->

                                                <p>Categoria de análise: Perfil dos Docentes</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Titulação do corpo docente</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Regime de trabalho do corpo docente</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tempo de experiência de magistério superior ou experiência na educação profissional</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tempo de experiência profissional do corpo docente (fora do magistério)</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->

                                                <p>Categoria de análise: Condições de Trabalho</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Número de alunos por docente equivalente a tempo integral</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Número de alunos por turma em disciplina teórica</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Número médio de disciplinas por docente</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Pesquisa, produção científica e tecnológica</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Dimensão: Instalações Físicas
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body pre-scrollable max-size">
                                                <p>Categoria de análise: Instalações Gerais</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Sala de professores e sala de reuniões</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Gabinetes de trabalho para professores</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Sala de aula</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Acesso dos alunos a equipamentos de informática</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Registros Acadêmicos</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->

                                                <p>Categoria de análise: Biblioteca (fonte de consulta: PPC e PDI)</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Livros da bibliografia básica</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Livros da bibliografia complementar</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Periódicos especializados, indexados e correntes</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->

                                                <p>Categoria de análise: Instalações e Laboratórios Específicos (fonte de consulta: PDI, PPC, etc.)</p>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Indicador</th>
                                                                <th>Conceito</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Laboratórios especializados</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Infraestrutura e serviços dos laboratórios especializados</td>
                                                                <td>
                                                                    <select class="form-control" name="conceito">
                                                                        <option value="1" selected>1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="button" onclick="simulate()" class="btn btn-primary">Simular Nota</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php require $_SERVER['DOCUMENT_ROOT'].'/template/_script.php' ?>
    <script>
        function simulate() {
            var nota = 0;
            var i = 0;
            $('select[name=conceito]').each(function (e, v) {
                nota += parseInt($(v).val());
                i++;
            });
            nota = Math.round(nota/i);

            $('#div-notification').removeClass('alert-success alert-danger alert-warning');
            var notificationClass = '';
            if(nota > 3){
                notificationClass = 'alert-success';
            }else if(nota > 2){
                notificationClass = 'alert-warning';
            }else{
                notificationClass = 'alert-danger';
            }

            $('#custom-p').html('A nota da avaliação do MEC seria: ' + nota);
            $('#div-notification').addClass(notificationClass);
            $('#div-notification').removeClass('hidden');
            $('#div-notification').fadeOut();
            $('#div-notification').fadeIn();

        }
    </script>

</body>

</html>
